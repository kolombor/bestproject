from django.db import models


class Like(models.Model):
    user_id = models.PositiveSmallIntegerField()
    post_id = models.PositiveSmallIntegerField()
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.full_name
