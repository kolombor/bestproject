from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'load/image/$', views.load_image, name='load_image'),
]
