import random
import string
from django.shortcuts import render
from animalshelp.settings import BASE_DIR
from django.http import JsonResponse
from django.http import HttpResponse
from django.core import serializers
from django.db.utils import IntegrityError
from django.views.decorators.csrf import csrf_exempt


def generate_code_name():
    """
        Need for promo code
    """
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for p in range(100))


@csrf_exempt
def load_image(request):
    type_image = request.FILES['file'].name.split('.')[-1]
    f = request.FILES['file']
    file_name = generate_code_name()
    file_path = '/'.join([BASE_DIR, 'media', 'user_photo', '{new_name}.{new_type}'.format(new_name=file_name, new_type=type_image)])
    small_path = file_path = '/'.join(['/media', 'user_photo', '{new_name}.{new_type}'.format(new_name=file_name, new_type=type_image)])
    with open(file_path, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return JsonResponse({'url': small_path})
