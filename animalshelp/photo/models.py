from django.db import models
from baseuser.models import BaseUser


class Photo(models.Model):
    url = models.CharField(max_length=255)

    def __str__(self):
        return "{name}".format(name=self.url)
