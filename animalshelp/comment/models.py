from django.db import models
from baseuser.models import BaseUser


class Comment(models.Model):
    user_id = models.PositiveSmallIntegerField()
    post_id = models.PositiveSmallIntegerField()
    date = models.DateTimeField(auto_now=True)
    body = models.CharField(max_length=500)

    def __str__(self):
        return self.user.full_name
