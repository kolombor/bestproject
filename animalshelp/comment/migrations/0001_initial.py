# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('user_id', models.PositiveSmallIntegerField()),
                ('post_id', models.PositiveSmallIntegerField()),
                ('date', models.DateTimeField(auto_now=True)),
                ('body', models.CharField(max_length=500)),
            ],
        ),
    ]
