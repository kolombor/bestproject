# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='BaseUser',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('password', models.CharField(verbose_name='password', max_length=128)),
                ('last_login', models.DateTimeField(verbose_name='last login', null=True, blank=True)),
                ('is_superuser', models.BooleanField(verbose_name='superuser status', help_text='Designates that this user has all permissions without explicitly assigning them.', default=False)),
                ('full_name', models.CharField(max_length=500)),
                ('email', models.EmailField(unique=True, max_length=254, blank=True)),
                ('avatar', models.CharField(max_length=500)),
                ('address', models.CharField(max_length=500)),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now)),
                ('about', models.CharField(max_length=500)),
                ('phone', models.CharField(max_length=13)),
                ('role', models.CharField(max_length=25)),
                ('groups', models.ManyToManyField(verbose_name='groups', blank=True, related_query_name='user', help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', to='auth.Group')),
                ('user_permissions', models.ManyToManyField(verbose_name='user permissions', blank=True, related_query_name='user', help_text='Specific permissions for this user.', related_name='user_set', to='auth.Permission')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
