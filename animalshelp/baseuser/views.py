import json
import random
import string
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.http import HttpResponse
from django.core import serializers
from django.db.utils import IntegrityError
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.contrib.auth import logout
from animalshelp.settings import BASE_DIR
# apps
from .models import BaseUser
from .forms import BaseUserFrom


def generate_code_name():
    """
        Need for promo code
    """
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for p in range(100))


def load_image(request):
    type_image = request.FILES['avatar'].name.split('.')[-1]
    f = request.FILES['avatar']
    file_name = generate_code_name()
    file_path = '/'.join([BASE_DIR, 'media', 'user_photo', '{new_name}.{new_type}'.format(new_name=file_name, new_type=type_image)])
    small_path = '/'.join(['/media', 'user_photo', '{new_name}.{new_type}'.format(new_name=file_name, new_type=type_image)])
    with open(file_path, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return small_path


@require_http_methods(["POST"])
@csrf_exempt
def register_user(request):
    email = request.POST.get('email')
    full_name = request.POST.get('full_name')
    password = request.POST.get('password')
    avatar = load_image(request)
    address = request.POST.get('address')
    about = request.POST.get('about')
    phone = request.POST.get('phone')
    try:
        user = BaseUser.objects.create_user(email=email,
                                            full_name=full_name,
                                            password=password)
    except IntegrityError as error:
        return JsonResponse({'error': error.args[0]})
    else:
        user.address = address
        user.about = about
        user.phone = phone
        user.avatar = avatar
        user.save()
        return JsonResponse({'error': None})


@require_http_methods(["POST"])
@csrf_exempt
def user_login(request):
    email = request.POST.get('email')
    password = request.POST.get('password')
    user = authenticate(email=email, password=password)
    if user is not None:
        print('USER IS')
        login(request, user)
        return JsonResponse({'error': None,
                             'user_id': request.user.id,
                             'role': request.user.role})
    return JsonResponse({'error': 404})


@csrf_exempt
def user_logout(request):
    logout(request)
    return JsonResponse({'error': None})


class ShowProfile(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ShowProfile, self).dispatch(*args, **kwargs)

    def get(self, request, user_id):
        user_id = int(user_id)
        if user_id == 0 and request.user.is_authenticated():
            return JsonResponse(json.loads(serializers.serialize("json", [request.user])), safe=False)
        if user_id == 0 and not request.user.is_authenticated():
            return JsonResponse({'error': 404})
        users = BaseUser.objects.filter(id=user_id)
        if not users:
            return JsonResponse({'error': 404})
        return JsonResponse(json.loads(serializers.serialize("json", users)), safe=False)

    def post(self, request, user_id):
        user_id = int(user_id)
        if user_id == 0 and request.user.is_authenticated():
            form = BaseUserFrom(request.POST, instance=request.user)
            if form.is_valid():
                form.save()
                if request.FILES.get('avatar'):
                    request.user.update_avatar(request)
                request.user.save()
                return JsonResponse({'error': None})
            return JsonResponse(form.errors)
        return JsonResponse({'error': 400})
