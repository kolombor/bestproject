import os
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.conf import settings
import random
import json
import string
from animalshelp.settings import BASE_DIR

def generate_code_name():
    """
        Need for promo code
    """
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for p in range(100))


def load_image(request):
    type_image = request.FILES['avatar'].name.split('.')[-1]
    f = request.FILES['avatar']
    file_name = generate_code_name()
    file_path = '/'.join([BASE_DIR, 'media', 'user_photo', '{new_name}.{new_type}'.format(new_name=file_name, new_type=type_image)])
    small_path = '/'.join(['/media', 'user_photo', '{new_name}.{new_type}'.format(new_name=file_name, new_type=type_image)])
    with open(file_path, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return small_path


class UserManager(BaseUserManager):

    def create_user(self, email, full_name, password=None):
        """
        Creates and saves a User with the given
            email,
            full_name,
            and password.
        """
        if not email:
            raise ValueError('Users must have an email')

        user = self.model(
            full_name=full_name,
            email=self.normalize_email(email),
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, full_name, password):
        """
        Creates and saves a User with the given
            email,
            full_name,
            and password.
        """
        user = self.create_user(
            password=password,
            email=email,
            full_name=full_name
        )
        user.is_admin = True
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class BaseUser(AbstractBaseUser, PermissionsMixin):
    """ BaseUser """
    full_name = models.CharField(max_length=500)
    email = models.EmailField(blank=True, unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['full_name']

    avatar = models.CharField(max_length=500)
    address = models.CharField(max_length=500)
    date_joined = models.DateTimeField(default=timezone.now)
    about = models.CharField(max_length=2000)
    phone = models.CharField(max_length=13)
    role = models.CharField(max_length=25)
    is_staff = models.BooleanField(default=False)

    objects = UserManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        return "%s %s" % (self.email, self.full_name)

    def get_short_name(self):
        return "%s" % self.full_name

    def is_staff(self):
        return self.is_staff

    def update_avatar(self, request):
        try:
            os.remove(settings.BASE_DIR+self.avatar)
        except:
            pass
        self.avatar = load_image(request)

    def to_json(self):
        return {'email': self.email,
                'full_name': self.full_name,
                'avatar': self.avatar,
                'address': self.address,
                'date_joined': self.date_joined.strftime('%Y-%m-%d'),
                'about': self.about,
                'phone': self.phone,
                'role': self.role}
