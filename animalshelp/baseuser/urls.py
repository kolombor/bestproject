from django.conf.urls import include, url
from . import views


urlpatterns = [
    url(r'register/$', views.register_user, name='register_user'),
    url(r'profile/(?P<user_id>\d+)/show/$', views.ShowProfile.as_view(), name='show_profile'),
    url(r'login/$', views.user_login, name='user_login'),
    url(r'logout/$', views.user_logout, name='user_logout'),
]
