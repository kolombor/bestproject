from django import forms
from .models import BaseUser


class BaseUserFrom(forms.ModelForm):

    class Meta:
        model = BaseUser
        fields = ['full_name', 'address', 'phone', 'about']
