from django.shortcuts import render

import random
import string
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.http import HttpResponse
from django.core import serializers
from django.db.utils import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
# apps
from baseuser.models import BaseUser
from post.models import Post
from .models import Order


@require_http_methods(['POST'])
@login_required
@csrf_exempt
def create_order(request):
    try:
        post = Post.objects.get(id=int(request.POST.get('post_id', 0)))
    except ObjectDoesNotExist:
        return HttpResponse(status=404)

    order = Order(user=request.user,
                  post=post,
                  description=request.POST.get('description'))
    order.save()
    return JsonResponse({'success': True}, safe=False)


@require_http_methods(['DELETE'])
@login_required
@csrf_exempt
def delete_order(request, order_id):
    try:
        order = Order.objects.get(id=int(order_id))
    except ObjectDoesNotExist:
        return HttpResponse(status=404)
    if request.user.role == 'admin':
        order.delete()
        return JsonResponse({'success': True}, safe=False)
    return JsonResponse({'error': 'not admin'}, safe=False)


@require_http_methods(['POST'])
@login_required
@csrf_exempt
def close_order(request, order_id):
    try:
        order = Order.objects.get(id=int(order_id))
    except ObjectDoesNotExist:
        return HttpResponse(status=404)
    if request.user.role == 'admin':
        order.post.delete()
        order.delete()
        return JsonResponse({'success': True}, safe=False)
    return JsonResponse({'error': 'not admin'}, safe=False)


@require_http_methods(['GET'])
def show_all_orders(request):
    return JsonResponse([ order.to_json() for order in Order.objects.all()], safe=False)


@require_http_methods(['GET'])
def get_orders_by_user(self, user_id):
    try:
        user = BaseUser.objects.get(id=int(user_id))
    except ObjectDoesNotExist:
        return JsonResponse({'error': 'not user'})
    orders = Order.objects.filter(user=user)
    return JsonResponse([order.to_json() for order in orders], safe=False)
