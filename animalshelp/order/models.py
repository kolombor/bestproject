from django.db import models
from post.models import Post
from baseuser.models import BaseUser


class Order(models.Model):
    post = models.ForeignKey(Post, null=True)
    user = models.ForeignKey(BaseUser, null=True)
    valid = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now=True)
    description = models.CharField(max_length=2000)

    def __str__(self):
        return self.description[:20]

    def to_json(self):
        return {
        'id': self.id,
        'user': self.user.id,
        'post': self.post.to_json(),
        'valid': self.valid,
        'date': self.date.strftime("%d/%m/%Y"),
        'description': self.description
    }
