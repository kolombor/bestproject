from django.conf.urls import include, url
from . import views

urlpatterns = [
    # url(r'(?P<post_id>\d+)/show/$', views.show_post, name='show_post'),
    url(r'create/$', views.create_order, name='create_order'),
    url(r'(?P<order_id>\d+)/delete/$', views.delete_order, name='delete_order'),
    url(r'(?P<order_id>\d+)/close/$', views.close_order, name='close_order'),
    url(r'all/$', views.show_all_orders, name='show_all_orders'),
    url(r'user/(?P<user_id>\d+)/show/', views.get_orders_by_user, name='get_orders_by_user')
]
