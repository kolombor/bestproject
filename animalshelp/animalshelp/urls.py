"""animalshelp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.conf import settings
from django.contrib import admin
from animalshelp import views
from baseuser import urls as baseuser_urls
from comment import urls as comment_urls
from post import urls as post_urls
from like import urls as like_urls
from order import urls as order_urls
from utils import urls as utils_urls
from django.conf.urls.static import static

urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += [
    url(r'^user/', include(baseuser_urls)),
    url(r'^comment/', include(comment_urls)),
    url(r'^post/', include(post_urls)),
    url(r'^like/', include(like_urls)),
    url(r'^order/', include(order_urls)),
    url(r'^utils/', include(utils_urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'', views.get_index, name='get_index'),
]
