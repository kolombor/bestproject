from django.shortcuts import render


def get_index(request):
    print('user', request.user.is_authenticated())
    return render(request, 'index.html')
