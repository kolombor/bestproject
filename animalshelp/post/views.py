import random
import os
import string
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.http import HttpResponse
from django.core import serializers
from django.db.utils import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.views.generic import View
from django.utils.decorators import method_decorator
# apps
from baseuser.models import BaseUser
from .models import Post
from photo.models import Photo
from animalshelp.settings import BASE_DIR


def generate_code_name():
    """
        Need for promo code
    """
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for p in range(100))


def load_image(request_image, folder):
    type_image = request_image.name.split('.')[-1]
    f = request_image
    file_name = generate_code_name()
    file_path = '/'.join([BASE_DIR, 'media', folder, '{new_name}.{new_type}'.format(new_name=file_name, new_type=type_image)])
    small_path = '/'.join(['/media', folder, '{new_name}.{new_type}'.format(new_name=file_name, new_type=type_image)])
    with open(file_path, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return small_path


@require_http_methods(["POST"])
@login_required
@csrf_exempt
def create_post(request):
    post = Post(user=request.user,
                valid=False,
                description=request.POST.get('description'),
                name=request.POST.get('name'),
                age=request.POST.get('age'),
                animal_type=request.POST.get('animal_type'))
    post.save()

    for key, post_photo in request.FILES.items():
        photo = Photo(url=load_image(request.FILES[key], 'post_photos'))
        photo.save()
        post.photos.add(photo)
    post.save()
    return JsonResponse({'error': None, 'post_id': post.id})

class ShowPost(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ShowPost, self).dispatch(*args, **kwargs)

    def get(self, request, post_id):
        try:
            post = Post.objects.get(id=post_id)
        except ObjectDoesNotExist:
            return JsonResponse({'error': 404})
        return JsonResponse({'post': post.to_json()})

    def post(self, request, post_id):
        try:
            post = Post.objects.get(id=post_id)
        except ObjectDoesNotExist:
            return JsonResponse({'error': 404})

        if request.user != post.user:
            return JsonResponse({'error': 405})
        
        post.valid = False
        post.description = request.POST.get('description')
        post.name = request.POST.get('name')
        post.age = request.POST.get('age')
        post.animal_type = request.POST.get('animal_type')
        post.save()

        if request.FILES:
            for photo in post.photos.all():
                try:
                    os.remove(settings.BASE_DIR+photo.url)
                except:
                    pass
                photo.delete()
        for key, post_photo in request.FILES.items():
            photo = Photo(url=load_image(request.FILES[key], 'post_photos'))
            photo.save()
            post.photos.add(photo)
        post.save()
        return JsonResponse({'success': True})


def show_all(request):
    valid = True if request.GET.get('valid') == 'true' else False
    return JsonResponse({'posts': [post.to_json() for post in Post.objects.filter(valid=valid)]})


@require_http_methods(["DELETE"])
@login_required
@csrf_exempt
def delete_post(request, post_id):
    try:
        post = Post.objects.get(id=int(post_id))
    except ObjectDoesNotExist:
        return JsonResponse({'error': 404})
    if request.user.role == 'admin':
        post.delete()
        return JsonResponse({'success': True}, safe=False)
    return JsonResponse({'error': 401}, safe=False)

@login_required
def get_by_user(request):
    return JsonResponse({'posts': [post.to_json() for post in Post.objects.filter(user=request.user)]})


@require_http_methods(["POST"])
@login_required
@csrf_exempt
def post_valid(request, post_id):
    try:
        post = Post.objects.get(id=int(post_id))
    except ObjectDoesNotExist:
        return JsonResponse({'error': 404})

    if request.user.role == 'admin':
        post.valid = True if request.POST.get('valid') == 'true' else False
        post.save()
        return JsonResponse({'success': True})

    return JsonResponse({'error': 400})
