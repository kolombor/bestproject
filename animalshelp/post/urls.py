from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'create/$', views.create_post, name='create_post'),
    url(r'(?P<post_id>\d+)/show/$', views.ShowPost.as_view(), name='show_post'),
    url(r'(?P<post_id>\d+)/valid/$', views.post_valid, name='post_valid'),
    url(r'all/show/$', views.show_all, name='show_all'),
    url(r'user/get/$', views.get_by_user, name='get_posts_by_user'),
    url(r'(?P<post_id>\d+)/delete/$', views.delete_post, name='delete_post_by_id'),
]
