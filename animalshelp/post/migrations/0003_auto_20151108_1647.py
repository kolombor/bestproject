# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('post', '0002_auto_20151025_1430'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='valid',
            field=models.BooleanField(default=False),
        ),
    ]
