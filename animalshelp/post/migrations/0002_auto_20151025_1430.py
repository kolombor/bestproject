# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('post', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='comments',
            field=models.ManyToManyField(to='comment.Comment'),
        ),
        migrations.AlterField(
            model_name='post',
            name='likes',
            field=models.ManyToManyField(to='like.Like'),
        ),
        migrations.AlterField(
            model_name='post',
            name='photos',
            field=models.ManyToManyField(to='photo.Photo'),
        ),
    ]
