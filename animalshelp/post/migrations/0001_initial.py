# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('photo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('valid', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=2000)),
                ('name', models.CharField(max_length=300)),
                ('date_publish', models.DateTimeField(auto_now=True)),
                ('animal_type', models.CharField(max_length=50)),
                ('age', models.PositiveSmallIntegerField()),
                ('comments', models.ManyToManyField(related_name='post_comments', to='photo.Photo')),
                ('likes', models.ManyToManyField(related_name='likes_comments', to='photo.Photo')),
                ('photos', models.ManyToManyField(related_name='photo_comments', to='photo.Photo')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
