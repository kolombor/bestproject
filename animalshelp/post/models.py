import os
from animalshelp.settings import BASE_DIR
from django.db import models
from baseuser.models import BaseUser
from photo.models import Photo
from like.models import Like
from comment.models import Comment


class Post(models.Model):
    user = models.ForeignKey(BaseUser)
    valid = models.BooleanField(default=False)
    description = models.CharField(max_length=2000)
    name = models.CharField(max_length=300)
    photos = models.ManyToManyField(Photo)
    likes = models.ManyToManyField(Like)
    comments = models.ManyToManyField(Comment)
    date_publish = models.DateTimeField(auto_now=True)
    animal_type = models.CharField(max_length=50)
    age = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.name

    def to_json(self):
        return {
            'id': self.id,
            'user': self.user.id,
            'valid': self.valid,
            'description': self.description,
            'name': self.name,
            'photos': [photo.url for photo in self.photos.all()],
            'date_publish': self.date_publish.strftime('%d/%m/%Y'),
            'animal_type': self.animal_type,
            'age': self.age}

    def delete(self):
        for img in self.photos.all():
            os.remove(BASE_DIR+img.url)
            img.delete()
        for like in self.likes.all():
            like.delete()
        return super(Post, self).delete()
