'use strict'

export default class Loader extends React.Component {
    
  render() {
    return (
      <section className="loader">
        <section className="loader_img"></section>
      </section>
    );
  }

}