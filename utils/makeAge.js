'use strict'

export default (age) => {
  switch (age) {
    case 0:
      return '< 1 місяця';
    case 1:
      return '1 місяць';
    case 2:
      return '2 місяця';
    case 3:
      return '3 місяця';
    case 6:
      return '6 місяців';
    case 12:
      return '1 рік';
    case 13:
      return '> 1 року';
  }
}