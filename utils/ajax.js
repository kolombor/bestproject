import noty from 'noty';

export default (url, data, func, method, contentType) => {
  console.log(data, url, typeof data);
  $.ajax({
    url: url,
    method: method || 'POST',
    data: data,
    processData: (contentType === false) ? false : true,
    contentType: (contentType === false) ? false : 'application/x-www-form-urlencoded',
    success: (data) => {
      console.log('success: ', data);
      if (func) {
        func(data);
      }
    },
    error: (data) => {
      var n = noty({
        text: 'Невідома помилка напишіть нам як це трапилося і ми це виправимо iqsk81ad@gmail.com',
        type: 'warning',
        animation: {
          open: 'animated bounceInLeft',
          close: 'animated bounceOutLeft',
          easing: 'swing',
          speed: 500
        }
      });
    },
  });
}