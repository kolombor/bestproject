import ajax from './ajax';
import Loader from './loader';
import removeLongWords from './removeLongWords';
import makeAge from './makeAge';
import makeType from './makeType';

module.exports = {
  ajax: ajax,
  Loader: Loader,
  removeLongWords: removeLongWords,
  makeAge: makeAge,
  makeType: makeType,
};