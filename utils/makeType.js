'use strict'

export default (type) => {
  switch (type) {
    case 'dog':
      return 'Собака';
    case 'cat':
      return 'Кішка';
    default:
      return 'Інший';
  }
}