export default (str) => {
  let words = str.split(/[\n]/);
  str = "";
  for (let key in words) {
    let _words = words[key].split(/[\s]/);
    let _str = "";
    for (let _key in _words) {
      if (_words[_key].length < 21) {
        _str += _words[_key] + ((_key == _words.length - 1) ? '' : ' ');
      } 
    }
    str += _str + ((key == words.length - 1) ? '' : '\n');
  }
  return str;
}