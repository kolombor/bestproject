'use strict'

import dispatcher from 'dispatcher';

export default class NoMatch extends React.Component {

  constructor() {
    super();
    dispatcher['noMatch'] = this;
    this.state = {

    };
  }
  
  render() {
    return (
      <div className="error-page frame__row_content" style={{position: 'relative'}}>
        <div className="error-page__base">
        <img src="/static/img/dog.svg" alt="" width="200" height="200" />
        <h1 className="error-page__title">Ви шукаєте те, чого немає на сайті!<br />Будь ласка, спробуйте ще!</h1>
        </div>
      </div>
    );
  }

}