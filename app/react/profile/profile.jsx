'use strict'

import dispatcher, {mainComponents} from 'dispatcher';
import {ajax} from 'utils';
import Registration from '../registration/registration';

export default class Profile extends React.Component {

  constructor() {
    super();
    dispatcher['profile'] = this;
    this.state = {
      profileData: null,
      edit: false,
    };
  }

  componentWillMount() {
    ajax (
      `/user/profile/${mainComponents.props.params.id}/show/`,
      '',
      this.setProfileData,
      'GET'
    );
  }

  setProfileData = (data) => {
    this.setState({
      profileData: data[0].fields,
    });
  }

  editProfile = () => {
    this.setState({
      edit: true,
    });
  }

  changeProfile = () => {
    this.state.edit = false;
    this.componentWillMount();
  }

  checkSelfProfile() {
    if (mainComponents.props.params.id == window.login) {
      return (
        <div className="profile__action">
          <button type="button" className="button button_green" onClick={this.editProfile}>Редагувати</button>
        </div>
      );
    }
  }
  
  render() {
    if (this.state.profileData) {
      if (this.state.edit) {
        return (
          <Registration data={this.state.profileData} />
        );
      } else {
        return (
          <section className="profile">
            <div className="avatar">
              <img src={this.state.profileData.avatar} width="200" />
            </div>
            <div className="profile__item">
              <h4 className="profile__title">Ім'я</h4>
              <p className="profile__data">{this.state.profileData.full_name}</p>
            </div>
            <div className="profile__item">
              <h4 className="profile__title">Адреса</h4>
              <p className="profile__data">{this.state.profileData.address}</p>
            </div>
            <div className="profile__item">
              <h4 className="profile__title">Телефонний номер</h4>
              <p className="profile__data">{this.state.profileData.phone}</p>
            </div>
            <div className="profile__item">
              <h4 className="profile__title">E-mail</h4>
              <p className="profile__data">{this.state.profileData.email}</p>
            </div>
            <div className="profile__item">
              <h4 className="profile__title">Інтереси</h4>
              <p className="profile__data">{this.state.profileData.about}</p>
            </div>
            {
              this.checkSelfProfile()
            }
            <div className="divider"></div>
          </section>
        );
      }
    } else {
      return (
        null
      );
    }
  }

}