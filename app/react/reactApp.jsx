'use strict'

import '../scss/main.scss';
import {Router, Route, IndexRoute} from 'react-router';

import MainComponents from './mainComponents/mainComponents';
import Home from './home/home';
import Registration from './registration/registration';
import Profile from './profile/profile';
import CreatePost from './createPost/createPost';
import Post from './post/post';
import About from './about/about';
import ValidatePost from './validatePost/validatePost';
import ValidateOrder from './validateOrder/validateOrder';
import Animals from './animals/animals';
import NoMatch from './noMatch/noMatch';

import createBrowserHistory from 'history/lib/createBrowserHistory'

let history = createBrowserHistory();

let routes = (
  <Router>
    <Route path='/' component={MainComponents}>
      <IndexRoute component={Home} />
      <Route path='registration' component={Registration} />
      <Route path='profile/:id' component={Profile} />
      <Route path='createPost' component={CreatePost} />
      <Route path='post/:id' component={Post} />
      <Route path='about' component={About} />
      <Route path='admin'>
        <Route path='/validatePost' component={ValidatePost} />
        <Route path='/validateOrder' component={ValidateOrder} />
      </Route>
      <Route path='animals' component={Animals} />
      <Route path="*" component={NoMatch}/>
    </Route>
  </Router>
);

window.onload = () => {
  try {
    ReactDOM.render (
      <Router history={history}>{routes}</Router>,
      document.getElementById('application')
    );
  } catch (e) {
    console.log(e);
  }

}