'use strict'

import dispatcher from 'dispatcher';

export default class About extends React.Component {

  constructor() {
    super();
    dispatcher['about'] = this;
    this.state = {

    };
  }
  
  render() {
    return (
      <div className="about-us">
        <div className="about-us__title">Про нашу діялність</div>
        <div className="about-us__engage">
          <ul className="element">
            <li className="element__item">
              <img className="about-us__pic" src="/static/slide1.jpg" width="310" height="210" alt="Foto engage" />
            </li>
            <li className="element__item">
              <div className="about-us__info">
                <div className="about-us__note">Дитяча Асоціація</div>
                <div className="about-us__purpose">
                  Дитяча міська громадська організація «Асоціація захисту тварин» - дипломант Всеукраїнської недержавної програми «Жива планета ХХІ<br /> сторіччя» в номінації «Найкращі<br /> гуманітарні здобутки».
                </div>
                <div className="about-us__quotes">" "</div>
              </div>
            </li>
            <li className="element__item">
              <img className="about-us__pic" src="/static/slide2.jpg" width="310" height="210" alt="Foto engage" />
            </li>
            <li className="element__item">
              <div className="about-us__info">
                <div className="about-us__note">Мета</div>
                <div className="about-us__purpose">
                  Виховання любові та гуманного ставлення<br /> до тварин, формування екологічної<br /> свідомості у дітей, допомога<br /> безпритульним тваринам.
                </div>
                <div className="about-us__quotes">" "</div>
              </div>
            </li>
            <li className="element__item">
              <img className="about-us__pic" src="/static/slide4.jpg" width="310" height="210" alt="Foto engage" />
            </li>
            <li className="element__item">
              <div className="about-us__info">
                <div className="about-us__note">Конкурси</div>
                <div className="about-us__purpose">
                  Організатор конкурсів дитячих творчих робіт «Мій ласкавий і ніжний звір», «Мій домашній улюбленець», а також виставок<br />тварин, екологічного проекту <br /> «Добро в твоєму серці».
                </div>
                <div className="about-us__quotes">" "</div>
              </div>
            </li>
            <li className="element__item">
              <img className="about-us__pic" src="/static/slide5.jpg" width="310" height="210" alt="Foto engage" />
            </li>
            <li className="element__item">
              <div className="about-us__info">
                <div className="about-us__note">Піклування</div>
                <div className="about-us__purpose">
                  За роки свого існування вихованцями Асоціації підібрано та пристроєно 700 безпритульних тварин.
                </div>
                <div className="about-us__quotes">" "</div>
              </div>
            </li>
            <li className="element__item">
              <img className="about-us__pic" src="/static/slide6.jpg" width="310" height="210" alt="Foto engage" />
            </li>
          </ul>
        </div>
        <div className="divider"></div>
      </div>
    );
  }

}