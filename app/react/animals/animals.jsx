'use strict'

import dispatcher, {mainComponents} from 'dispatcher';
import {ajax} from 'utils';

export default class Animals extends React.Component {

  constructor() {
    super();
    dispatcher['animals'] = this;
    this.state = {
      animalsData: null,
      animalType: '',
      animalAge: '', 
    };
  }

  componentWillMount() {
    ajax (
      '/post/all/show/?valid=true',
      '',
      this.setAnimalsData,
      'GET'
    );
  }

  setAnimalsData = (data) => {
    this.setState({
      animalsData: data.posts,
    });
  }

  changeAnimalType = (e) => {
    this.setState({
      animalType: e.target.value,
    });
  }

  changeAnimalAge = (e) => {
    this.setState({
      animalAge: e.target.value,
    });
  }

  resetFilters = () => {
    this.setState({
      animalAge: '',
      animalType: '',
    });
  }

  filter() {
    let animals = [];
    this.state.animalsData.forEach((animal, key) => {
      if ((!this.state.animalType  || animal.animal_type == this.state.animalType) && (!this.state.animalAge || animal.age == this.state.animalAge)) {
        animals.push (
          <li className="element__item" key={key} onClick={() => {mainComponents.props.history.pushState(null, `/post/${animal.id}`, null)}}>
            <div className="panel">
              <img className="panel__img" src={animal.photos[0]} alt="" width="420" height="250" />
              <h2 className="panel__title">{animal.name}</h2>
              <p className="panel__text">{animal.description}</p>
            </div>
          </li>
        );
      }
    });
    if (animals.length) {
      return (
        <div className="notion">
          <div className="notion__main">
            <ul className="element">
              {
                animals
              }
            </ul>
          </div>
        </div>
      );
    } else {
      return (
        <div className="empty">
          <div className="empty__posts">
            Нічого не знайдено, будь ласка<br /> натисніть кнопку "Очистити фільтри" або<br /> зробі інший вибір
          </div>
        </div>
      );
    }
    
  }
  
  render() {
    if (this.state.animalsData) {
      return (
        <div className="filter-post">
          <div className="filter-post__title">Всі тварини</div>
          <div className="filter-post__main">
            <div className="filter-post__group">
              <label htmlFor="selectType" className="filter-post__note">Тип тварини</label>
              <div className="filter-post__select">
                <select value={this.state.animalType} className="select select_shape_stretch filter-post__search" onChange={this.changeAnimalType} id="selectType">
                  <option value="" disabled>Виберіть тип тварини</option>
                  <option value="dog">Собака</option>
                  <option value="cat">Кішка</option>
                  <option value="other">Інші</option>
                </select>
              </div>
            </div>
            <div className="filter-post__group">
              <label htmlFor="selectAge" className="filter-post__note">Вік тварини</label>
                <div className="filter-post__select">
                <select value={this.state.animalAge} className="select select_shape_stretch filter-post__search" id="selectAge" onChange={this.changeAnimalAge}>
                  <option value="" disabled>Виберіть вік тварини</option>
                  <option value="0">менше 1 місяця</option>
                  <option value="1">1 місяць</option>
                  <option value="2">2 місяця</option>
                  <option value="3">3 місяця</option>
                  <option value="6">6 місяців</option>
                  <option value="12">1 рік</option>
                  <option value="13">більше 1 року</option>
                </select>
              </div>
            </div>
          </div>
          <div className="filter-post__foot">
            <button className={"button button_blue" + (!this.state.animalType && !this.state.animalAge ? ' button_disabled' : '')} onClick={this.resetFilters}>Очистити фільтри</button>
          </div>
          <div className="divider"></div>
          {
            this.filter()
          }
        </div>
      );
    } else {
      return null;
    }
  }

}