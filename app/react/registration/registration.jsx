'use strict'

import dispatcher, {mainComponents, profile} from 'dispatcher';
import {ajax, removeLongWords} from 'utils';
import noty from 'noty';

export default class Header extends React.Component {

  constructor() {
    super();
    dispatcher['registration'] = this;
    this.state = {
      name: {
        valid: false,
        value: '',
      },
      address: {
        valid: false,
        value: '',
      },
      phone: {
        valid: false,
        value: '',
      },
      mail: {
        valid: false,
        value: '',
      },
      password: {
        valid: false,
        value: '',
      },
      passwordConfirm: {
        valid: false,
        value: '',
      },
      interests: {
        value: '',
      },
      avatar: {
        valid: false,
        value: '',
      },
    };
  }

  componentWillMount() {
    if (this.props.data) {
      this.state = {
        name: {
          valid: true,
          value: this.props.data.full_name,
        },
        address: {
          valid: true,
          value: this.props.data.address,
        },
        phone: {
          valid: true,
          value: this.props.data.phone,
        },
        mail: {
          valid: true,
          value: '',
        },
        password: {
          valid: true,
          value: '',
        },
        passwordConfirm: {
          valid: true,
          value: '',
        },
        interests: {
          value: this.props.data.about,
        },
        avatar: {
          valid: true,
          value: this.props.data.avatar,
        },
        button: true,
      }
    }
  }

  checkAvatar() {
    if (this.state.avatar.valid) {
      return (
        <img src={this.state.avatar.value} width="200" onDoubleClick={this.removeAvatar} />
      );
    } else {
      return (
        <label htmlFor="avatar" className="center avatar_label">
          <img src="/static/img/placeholder.png" width="200" height="200" />
        </label>
      );
    }
  }

  changeAvatar = (e) => {
    let file = e.target.files[0];
    let reader = new FileReader();
    reader.onload = ((theFile) => {
      return (e) => {
        this.setState({
          avatar: {
            valid: true,
            value: e.target.result,
          },
        });
      };
    })(file);
    reader.readAsDataURL(file);
  }

  removeAvatar = () => {
    this.setState({
      avatar: {
        valid: false,
        value: '',
      }
    });
  }

  changeName = (e) => {
    if (e.target.value.length < 30) {
      this.state.name.value = e.target.value;
      if (this.state.name.value.length > 4) {
        this.state.name.valid = true;
      } else {
        this.state.name.valid = false;
      }
      this.setState({
        name: this.state.name,
      });
    }
  }

  validName = (e) => {
    if (this.state.name.valid) {
      e.target.classList.remove('input_error');
    } else {
      e.target.classList.add('input_error');
    }
  }

  changeAddress = (e) => {
    if (e.target.value.length < 30) {
      this.state.address.value = e.target.value;
      if (this.state.address.value.length > 8) {
        this.state.address.valid = true;
      } else {
        this.state.address.valid = false;
      }
      this.setState({
        address: this.state.address,
      });
    }
  }

  validAddress = (e) => {
    if (this.state.address.valid) {
      e.target.classList.remove('input_error');
    } else {
      e.target.classList.add('input_error');
    }
  }

  changePhone = (e) => {
    if (e.target.value.length < 14) {
      this.state.phone.value = e.target.value;
      if (/^\+[1-9]{1}[0-9]{3,14}$/.test(this.state.phone.value)) {
        this.state.phone.valid = true;
      } else {
        this.state.phone.valid = false;
      }
      this.setState({
        phone: this.state.phone,
      });
    }
  }

  validPhone = (e) => {
    if (this.state.phone.valid) {
      e.target.classList.remove('input_error');
    } else {
      e.target.classList.add('input_error');
    }
  }

  changeMail = (e) => {
    if (e.target.value.length < 30) {
      this.state.mail.value = e.target.value;
      if (/^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/.test(this.state.mail.value)) {
        this.state.mail.valid = true;
      } else {
        this.state.mail.valid = false;
      }
      this.setState({
        mail: this.state.mail,
      });
    }
  }

  validMail = (e) => {
    if (this.state.mail.valid) {
      e.target.classList.remove('input_error');
    } else {
      e.target.classList.add('input_error');
    }
  }

  badMail() {
    this.setState({
      mail: {
        value: '',
        valid: false,
      },
    });
  }

  changePassword = (e) => {
    if (e.target.value.length < 30) {
      this.state.password.value = e.target.value;
      if (this.state.password.value.length > 5) {
        this.state.password.valid = true;
      } else {
        this.state.password.valid = false;
      }
      this.setState({
        password: this.state.password,
      });
    }
  }

  validPassword = (e) => {
    if (this.state.password.valid) {
      e.target.classList.remove('input_error');
    } else {
      e.target.classList.add('input_error');
    }
  }

  changePasswordConfirm = (e) => {
    if (e.target.value.length < 30) {
      this.state.passwordConfirm.value = e.target.value;
      if (this.state.passwordConfirm.value.length > 5 && (this.state.passwordConfirm.value === this.state.password.value)) {
        this.state.passwordConfirm.valid = true;
      } else {
        this.state.passwordConfirm.valid = false;
      }
      this.setState({
        passwordConfirm: this.state.passwordConfirm,
      });
    }
  }

  validPasswordConfirm = (e) => {
    if (this.state.passwordConfirm.valid) {
      e.target.classList.remove('input_error');
    } else {
      e.target.classList.add('input_error');
    }
  }

  changeInterests = (e) => {
    if (e.target.value.length < 500) {
      this.setState({
        interests: {
          value: e.target.value,
        },
      });
    }
  }

  register = () => {
    $('.loader')[0].style.display = 'block';
    let data = new FormData();
    if (ReactDOM.findDOMNode(this.refs.avatar).files[0]) {
      data.append('avatar', ReactDOM.findDOMNode(this.refs.avatar).files[0]);
    }
    data.append('full_name', removeLongWords(this.state.name.value));
    data.append('about', removeLongWords(this.state.interests.value));
    data.append('address', removeLongWords(this.state.address.value));
    data.append('phone', this.state.phone.value);
    if (this.props.data) {
      ajax (
        '/user/profile/0/show/',
        data,
        (data) => {
          $('.loader')[0].style.display = 'none';
          profile.changeProfile();
        },
        'POST',
        false
      );
    } else {
      data.append('password', this.state.password.value);
      data.append('email', this.state.mail.value);
      ajax (
        '/user/register/',
        data,
        (data) => {
          $('.loader')[0].style.display = 'none';
          if (data.error === null) {
            this.goLogin();
          } else if (data.error === 1062) {
            this.badMail();
            var n = noty({
              text: 'Така пошта вже зареєстрована',
              type: 'warning',
              animation: {
                open: 'animated bounceInLeft',
                close: 'animated bounceOutLeft',
                easing: 'swing',
                speed: 500
              },
              timeout: 5000,
            });
          } else {
            var n = noty({
              text: 'Невідома помилка напишіть нам як це трапилося і ми це виправимо iqsk81ad@gmail.com',
              type: 'warning',
              animation: {
                open: 'animated bounceInLeft',
                close: 'animated bounceOutLeft',
                easing: 'swing',
                speed: 500
              },
              timeout: 5000,
            });
          }
        },
        'POST',
        false
      );
    }
  }

  goLogin = (data) => {        
    ajax (
      '/user/login/',
      {
        email: this.state.mail.value,
        password: this.state.password.value,
      },
      (data) => {
        window.login = data.user_id;
        window.role = data.role;
        mainComponents.props.history.pushState(null, `/profile/${window.login}`, null);
        mainComponents.update();
      }
    );
  }

  checkEditFields() {
    if (!this.props.data) {
      return [
        <div className="input-group" key={0}>
          <label className="input-group__title" htmlFor="email">E-mail</label>
          <input value={this.state.mail.value} className="input" type="email" id="email" placeholder="Введіть Вашу електронну пошту" onChange={this.changeMail} onBlur={this.validMail} />
        </div>,
        <div className="input-group" key={1}>
          <label className="input-group__title" htmlFor="password">Пароль</label>
          <input value={this.state.password.value} className="input" type="password" id="password" placeholder="Введіть Ваш пароль, будьте уважні" onChange={this.changePassword} onBlur={this.validPassword} />
        </div>,
        <div className="input-group" key={2}>
          <label className="input-group__title" htmlFor="passwordCheck">Перевірка пароля</label>
          <input value={this.state.passwordConfirm.value} className="input" type="password" id="passwordCheck" placeholder="Введіть Ваш пароль ще раз, будьте уважні" onChange={this.changePasswordConfirm} onBlur={this.validPasswordConfirm} />
        </div>,
      ];
    }
  }
  
  render() {
    return (
      <section className="register">
        <h1 className="register__title">{this.props.data ? 'Редагувати' : 'Реєстрація'}</h1>
        <div>
          <div className="avatar">
            {
              this.checkAvatar()
            }
            <input style={{display: 'none'}} ref="avatar" id="avatar" accept="image/jpeg,image/png" type="file" onChange={this.changeAvatar} />
          </div>
          <div className="input-group">
            <label className="input-group__title" htmlFor="name">Ім'я</label>
            <input value={this.state.name.value} className="input" type="text" id="name" placeholder="Введіть Ваше ім,я" onChange={this.changeName} onBlur={this.validName} />
          </div>
          <div className="input-group">
            <label className="input-group__title" htmlFor="address">Адреса</label>
            <input value={this.state.address.value} className="input" type="text" id="address" placeholder="Введіть Вашу адресу" onChange={this.changeAddress} onBlur={this.validAddress} />
          </div>
          <br />
          <div className="input-group">
            <label className="input-group__title" htmlFor="tel">Телефонний номер</label>
            <input value={this.state.phone.value} className="input" type="tel" id="tel" placeholder="Введіть Ваш номер телефону" onChange={this.changePhone} onBlur={this.validPhone} />
          </div>
          {
            this.checkEditFields()
          }
          <div className="input-group">
            <label className="input-group__title" htmlFor="interests">Інтереси</label>
            <textarea value={this.state.interests.value} className="input input_textarea" type="password" id="interests" placeholder="Введіть ваші інтереси, так ми будемо розуміти, що вас цікавить" onChange={this.changeInterests}></textarea>
          </div>
          <br />
          <button className={"register__button button button_green" + (this.state.name.valid && this.state.address.valid && this.state.phone.valid && this.state.mail.valid && this.state.passwordConfirm.valid && this.state.avatar.valid ? '' : ' button_disabled')} onClick={this.register}>{this.props.data ? 'Зберегти' : 'Зареєструватись'}</button>
          <div className="divider"></div>
        </div>
      </section>
    );
  }

}