'use strict'

import dispatcher, {mainComponents} from 'dispatcher';
import {ajax} from 'utils';
import {Link} from 'react-router';

export default class ValidatePost extends React.Component {

  constructor() {
    super();
    dispatcher['validatePost'] = this;
    this.state = {
      postsData: null,
    };
  }

  componentWillMount() {
    ajax (
      '/post/all/show/?valid=false',
      '',
      this.setPostsData,
      'GET'
    );
  }

  setPostsData = (data) => {
    this.setState({
      postsData: data.posts,
    });
  }

  makeOrders() {
    if (this.state.postsData.length) {
      return this.state.postsData.map((post, key) => {
        return (
          <div className="post" key={key}>
            <Link to={`/profile/${post.user}`}>Автор поста</Link>
            <div className="name">{post.name}</div>
            <button className="button button_green" onClick={() => {mainComponents.props.history.pushState(null, `/post/${post.id}`, null)}}>Переглянути пост</button>
          </div>
        );
      })  
    } else {
      return (
        <h2>Немає неопублікованих постів</h2>
      );
    }
  }
  
  render() {
    if (this.state.postsData) {
      return (
        <div className="validPost">
          {
            this.makeOrders()
          }
        </div>
      );
    } else {
      return null;
    }
  }
}