'use strict'

import dispatcher from 'dispatcher';
import {ajax} from 'utils';
import {Link} from 'react-router';

export default class ValidateOrder extends React.Component {

  constructor() {
    super();
    dispatcher['validateOrder'] = this;
    this.state = {
      ordersData: null,
    };
  }

  componentWillMount() {
    ajax (
      '/order/all/',
      '',
      this.setOrdersData,
      'GET'
    );
  }

  setOrdersData = (data) => {
    this.setState({
      ordersData: data,
    });
  }

  closeOrder = (id, key) => {
    ajax (
      `/order/${id}/close/`,
      '',
      (data) => {
        if (data.error) {
          var n = noty({
            text: 'Тимчасово ця функція недоступна, спробуйте пізніше або напишіть нам про проблему iqsk81ad@gmail.com',
            type: 'warning',
            animation: {
              open: 'animated bounceInLeft',
              close: 'animated bounceOutLeft',
              easing: 'swing',
              speed: 500
            },
            timeout: 5000,
          });
        } else {
          var n = noty({
            text: 'Заявка оформлена успішно',
            type: 'success',
            animation: {
              open: 'animated bounceInLeft',
              close: 'animated bounceOutLeft',
              easing: 'swing',
              speed: 500
            },
            timeout: 5000,
          });
          this.state.ordersData.splice(key, 1);
          this.setState({
            ordersData: this.state.ordersData,
          });
        }
      },
      'POST'
    );
  }

  deleteOrder = (id, key) => {
    ajax (
      `/order/${id}/delete/`,
      '',
      (data) => {
        if (data.error) {
          var n = noty({
            text: 'Тимчасово ця функція недоступна, спробуйте пізніше або напишіть нам про проблему iqsk81ad@gmail.com',
            type: 'warning',
            animation: {
              open: 'animated bounceInLeft',
              close: 'animated bounceOutLeft',
              easing: 'swing',
              speed: 500
            },
            timeout: 5000,
          });
        } else {
          var n = noty({
            text: 'Заявка успішно видалена',
            type: 'success',
            animation: {
              open: 'animated bounceInLeft',
              close: 'animated bounceOutLeft',
              easing: 'swing',
              speed: 500
            },
            timeout: 5000,
          });
          this.state.ordersData.splice(key, 1);
          this.setState({
            ordersData: this.state.ordersData,
          });
        }
      },
      'DELETE'
    );
  }

  makeOrders() {
    if (this.state.ordersData.length) {
      return this.state.ordersData.map((order, key) => {
        return (
          <div className="order" key={key}>
            <div className="date">{order.date}</div>
            <div className="author"><Link to={`/profile/${order.post.user}`}>Автор поста</Link></div>
            <div className="_post"><Link to={`/post/${order.post.id}`}>{order.post.name}</Link></div>
            <div className="author"><Link to={`/profile/${order.user}`}>Автор заявки</Link></div>
            <div className="desc">{order.description}</div>
            <button className="button button_green" onClick={this.closeOrder.bind(null, order.id, key)}>Закрити ордер</button>
            <button className="button button_red" onClick={this.deleteOrder.bind(null, order.id, key)}>Видалити ордер</button>
          </div>
        );
      });
    } else {
      return (
        <h2>Нема нових заявок</h2>
      );
    }
  }
  
  render() {
    if (this.state.ordersData) {
      return (
        <div className="validOrders">
          {
            this.makeOrders() 
          }
        </div>
      );
    } else {
      return null;
    }
  }

}