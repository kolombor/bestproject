'use strict'

import dispatcher, {mainComponents, post} from 'dispatcher';
import {ajax, removeLongWords} from 'utils';
import noty from 'noty';

export default class CreatePost extends React.Component {

  constructor() {
    super();
    dispatcher['createPost'] = this;
    this.state = {
      photos: [],
      badPhotos: [],
      name: {
        valid: false,
        value: '',
      },
      animalType: '',
      animalAge: '', 
      description: '',
    };
  }

  componentWillMount() {
    if (this.props.data) {
      this.setState({
        photos: this.props.data.photos,
        name: {
          valid: true,
          value: this.props.data.name,
        },
        animalType: this.props.data.animal_type,
        animalAge: this.props.data.age, 
        description: this.props.data.description,
      });
    }
  }

  componentDidUpdate() {
    $('.pics').glisse({
      changeSpeed: 550, 
      speed: 500,
      effect:'bounce',
      fullscreen: true
    });
  }

  changePhotos = (e) => {
    let files = e.target.files;
    let file, reader;
    for (let i = 0; i < files.length ; i++) {
      file = files[i];
      reader = new FileReader();
      reader.onload = (theFile) => {
        let img = document.createElement('img');
        img.onload = () => {
          if (img.width < 400 || img.height < 400 || img.width / img.height < 1) {
            console.log('image so low :(');
            this.state.badPhotos.push(i);
          } else {
            this.state.photos.push(theFile.target.result);
            if (this.state.photos.length === files.length - this.state.badPhotos.length) {
              this.setState({
                photos: this.state.photos,
              });
            }
          }
        };
        img.src = theFile.target.result;
      };
      reader.readAsDataURL(file);
    }
    var n = noty({
      text: 'Щоб переглянути всі фотографії натисніть на будь яку та керуйте стрілками клавіатури',
      type: 'information',
      animation: {
        open: 'animated bounceInLeft',
        close: 'animated bounceOutLeft',
        easing: 'swing',
        speed: 500
      },
      timeout: 5000,
    });
  }

  resetPhotos = () => {
    this.setState({
      photos: [],
      badPhotos: [],
    });
    ReactDOM.findDOMNode(this.refs.photos).files = [];
    ReactDOM.findDOMNode(this.refs.photos).value = '';
  }

  changeName = (e) => {
    if (e.target.value.length < 30) {
      this.state.name.value = e.target.value;
      if (this.state.name.value.length > 4) {
        this.state.name.valid = true;
      } else {
        this.state.name.valid = false;
      }
      this.setState({
        name: this.state.name,
      });
    }
  }

  validName = (e) => {
    if (this.state.name.valid) {
      e.target.classList.remove('input_error');
    } else {
      e.target.classList.add('input_error');
    }
  }

  changeAnimalType = (e) => {
    this.setState({
      animalType: e.target.value,
    });
  }

  changeAnimalAge = (e) => {
    this.setState({
      animalAge: e.target.value,
    });
  }

  changeDescription = (e) => {
    if (e.target.value.length < 500) {
      this.setState({
        description: e.target.value,
      });
    }
  }

  createPost = () => {
    $('.loader')[0].style.display = 'block';
    let data = new FormData();
    let photos = [];
    console.log(ReactDOM.findDOMNode(this.refs.photos).files.length, '!!!');
    for (let key = 0; key < ReactDOM.findDOMNode(this.refs.photos).files.length; key++) {
      if (this.state.badPhotos.indexOf(key) < 0) {
        data.append(`photo_${key}`, ReactDOM.findDOMNode(this.refs.photos).files[key]);
      }
    }
    data.append('name', removeLongWords(this.state.name.value));
    data.append('description', removeLongWords(this.state.description));
    data.append('animal_type', this.state.animalType);
    data.append('age', this.state.animalAge);
    if (this.props.data) {
      ajax (
        `/post/${this.props.data.id}/show/`,
        data,
        (data) => {
          $('.loader')[0].style.display = 'none';
          if (!data.error) {
            post.changePost();
          } else {
            var n = noty({
              text: 'Невдалося редагувати пост, спробуйте пізніше та напишіть нам про проблему iqsk81ad@gmail.com',
              type: 'warning',
              animation: {
                open: 'animated bounceInLeft',
                close: 'animated bounceOutLeft',
                easing: 'swing',
                speed: 500
              },
              timeout: 5000,
            });
          }
        },
        'POST',
        false
      );
    } else {
      ajax (
        '/post/create/',
        data,
        (data) => {
          $('.loader')[0].style.display = 'none';
          if (!data.error) {
            var n = noty({
              text: 'Пост успішно створенний',
              type: 'success',
              animation: {
                open: 'animated bounceInLeft',
                close: 'animated bounceOutLeft',
                easing: 'swing',
                speed: 500
              },
              timeout: 5000,
            });
            this.props.history.pushState(null, `/post/${data.post_id}`, null);
          } else {
            var n = noty({
              text: 'Невдалося створити пост, спробуйте пізніше або напишіть нам про проблему iqsk81ad@gmail.com',
              type: 'warning',
              animation: {
                open: 'animated bounceInLeft',
                close: 'animated bounceOutLeft',
                easing: 'swing',
                speed: 500
              },
              timeout: 5000,
            });
          }
        },
        'POST',
        false
      );
    }
  }

  endEdit = () => {
    post.setState({
      edit: false,
    });
  }

  checkPhotos() {
    if (this.state.photos.length) {
      return this.state.photos.map((photo, key) => {
        return (
          <img style={{display: key ? 'none' : 'block'}} src={photo} key={key} width="600" className="pics" data-glisse-big={photo} rel="group1" />
        );
      });
    } else {
      return (
        <label className="post__upload post__upload__label" htmlFor="photos">
          <i className="post__icon"></i>
          <span className="post__title">Завантажте, будь ласка, зображення</span>
        </label>
      );
    }
  }

  checkPhotosReset = () => {
    if (this.state.photos.length) {
      let style = {
        position: 'relative',
        left: '50%',
        margin: '20px -112px 0',
      };
      return (
        <button style={style} type="button" className="button button_yellow" onClick={this.resetPhotos}>Видалити зображення</button>
      );
    }
  }
  
  render() {
    return (
      <main>
        <div className="post">
          {
            this.checkPhotos()
          }
          {
            this.checkPhotosReset()
          }
          <input style={{display: 'none'}} ref="photos" id="photos" accept="image/jpeg,image/png" type="file" multiple onChange={this.changePhotos} />
          <div className="post-group">
            <label className="post-group__title" htmlFor="name">Зазначте назву поста</label>
            <input value={this.state.name.value} id="name" className="post-group__input input" type="text" onBlur={this.validName} onChange={this.changeName} />
          </div>
          <div className="post-group">
            <label className="post-group__title" htmlFor="selectType">Виберіть тип тварини</label>
            <select value={this.state.animalType} className="post-group__input input" id="selectType" onChange={this.changeAnimalType}>
              <option value="" disabled>Виберіть тип тварини</option>
              <option value="dog">Собака</option>
              <option value="cat">Кішка</option>
              <option value="other">Інші</option>
            </select>
          </div>
          <div className="post-group">
            <label className="post-group__title" htmlFor="selectAge">Виберіть вік тварини</label>
            <select value={this.state.animalAge} className="post-group__input input" id="selectAge" onChange={this.changeAnimalAge}>
              <option value="" disabled>Виберіть вік тварини</option>
              <option value="0">менше 1 місяця</option>
              <option value="1">1 місяць</option>
              <option value="2">2 місяця</option>
              <option value="3">3 місяця</option>
              <option value="6">6 місяців</option>
              <option value="12">1 рік</option>
              <option value="13">більше 1 року</option>
            </select>
          </div>
          <div className="post-group">
            <label className="post-group__title" htmlFor="description">Зазначте опис</label>
            <textarea value={this.state.description} className="post-group__textarea input" id="description" cols="30" rows="4" onChange={this.changeDescription}></textarea>
          </div>
          <div className="post__button">
            <button type="button" className={"button button_green" + (this.state.name.valid && this.state.photos.length && this.state.animalType && this.state.animalAge ? '' : ' button_disabled')} onClick={this.createPost}>Зберегти</button>
            {
              this.props.data ? <button style={{marginLeft: '20px'}} type="button" className="button button_blue" onClick={this.endEdit}>Назад до перегляду</button> : null
            }
          </div>
        </div>
      </main>
    );
  }

}