'use strict'

import dispatcher, {mainComponents} from 'dispatcher';

export default class Home extends React.Component {

  constructor() {
    super();
    dispatcher['home'] = this;
  }
  
  render() {
    return (
      <div>
        <section className="banner">
          <h1 className="banner__title">Добро у твоєму серці</h1>
          <a className="link link_absolute link_redirect_limpid" onClick={() => {mainComponents.props.history.pushState(null, '/animals', null)}}>Переглянути тварин</a>
        </section>
        <section className="features">
            <div className="features__row">
              <div className="features__cell">
                <div className="features__image"><img src="/static/img/features1.svg" alt="" /></div>
              </div>
              <div className="features__cell">
                <div className="features__image"><img src="/static/img/features2.svg" alt="" /></div>
              </div>
              <div className="features__cell">
                <div className="features__image"><img src="/static/img/features3.svg" alt="" /></div>
              </div>
            </div>
            <div className="features__row">
              <div className="features__cell">
                <div className="features__title">
                  <span className="features__title_text">Добро починається<br /> з тебе</span>
                </div>
              </div>
              <div className="features__cell">
                <div className="features__title">
                  <span className="features__title_text">Ми відповідаємо за тих,<br /> кого приручили</span>
                </div>
              </div>
              <div className="features__cell">
                <div className="features__title">
                  <span className="features__title_text">Щоб повірити в добро,<br /> почни робити його</span>
                </div>
              </div>
            </div>
        </section>
      </div>
    );
  }

}