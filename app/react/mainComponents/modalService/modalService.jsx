'use strict'

import dispatcher, {mainComponents} from 'dispatcher';
import SignIn from './modals/signIn';
import MakeOrder from './modals/makeOrder';

export default class ModalService extends React.Component {
  
  render() {
    return (
      <div className="modalService">
        <SignIn />
        <MakeOrder />
        <div className="loader" />
      </div>
    );
  }

}