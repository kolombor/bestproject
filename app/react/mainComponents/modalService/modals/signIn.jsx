'use strict'

import {mainComponents} from 'dispatcher';
import {ajax} from 'utils';
import noty from 'noty';

export default class SignIn extends React.Component {

  constructor() {
    super();
    this.state = {
      password: {
        valid: false,
        value: '',
      },
      mail: {
        valid: false,
        value: '',
      }
    };
  }

  changeMail = (e) => {
    if (e.target.value.length < 30) {
      this.state.mail.value = e.target.value;
      if (/^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/.test(this.state.mail.value)) {
        this.state.mail.valid = true;
      } else {
        this.state.mail.valid = false;
      }
      this.setState({
        mail: this.state.mail,
      });
    }
  }

  validMail = (e) => {
    if (this.state.mail.valid) {
      e.target.classList.remove('input_error');
    } else {
      e.target.classList.add('input_error');
    }
  }

  changePassword = (e) => {
    if (e.target.value.length < 30) {
      this.state.password.value = e.target.value;
      if (this.state.password.value.length > 5) {
        this.state.password.valid = true;
      } else {
        this.state.password.valid = false;
      }
      this.setState({
        password: this.state.password,
      });
    }
  }

  validPassword = (e) => {
    if (this.state.password.valid) {
      e.target.classList.remove('input_error');
    } else {
      e.target.classList.add('input_error');
    }
  }

  reg = () => {
    document.getElementById('signIn').style.display = 'none';
    document.getElementById('lean_overlay').style.display = 'none';
    mainComponents.props.history.pushState(null, '/registration', null);
  }

  signIn = () => {       
    ajax (
      '/user/login/',
      {
        email: this.state.mail.value,
        password: this.state.password.value,
      },
      (data) => {
        if (data.error) {
          var n = noty({
            text: 'Неправильний пароль або email',
            type: 'warning',
            animation: {
              open: 'animated bounceInLeft',
              close: 'animated bounceOutLeft',
              easing: 'swing',
              speed: 500
            },
            timeout: 5000,
          });
        } else {
          window.login = data.user_id;
          window.role = data.role;
          // mainComponents.props.history.pushState(null, `/`, null);
          document.getElementById('signIn').style.display = 'none';
          document.getElementById('lean_overlay').style.display = 'none';
          mainComponents.update();
        }
      }
    );
  }
  
  render() {
    return (
      <div className="signIn" id="signIn">
        <h2>Вхід в систему</h2>
        <div className="input-group">
          <label className="input-group__title" htmlFor="mail">Email</label>
          <input value={this.state.mail.value} className="input" type="email" id="mail" placeholder="Введіть ваш email" onChange={this.changeMail} onBlur={this.validMail} />
        </div>
        <div className="input-group">
          <label className="input-group__title" htmlFor="password">Пароль</label>
          <input value={this.state.password.value} className="input" type="password" id="password" placeholder="Введіть ваш пароль" onChange={this.changePassword} onBlur={this.validPassword} />
        </div>
        <button className={"signIn__button button button_blue" + (this.state.mail.valid && this.state.password.valid ? '' : ' button_disabled')}onClick={this.signIn}>Ввійти</button>
        <br />
        <button className="register__button button button_green" onClick={this.reg}>Зареєструватись</button>
      </div>
    );
  }

}