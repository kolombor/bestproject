'use strict'

import dispatcher, {mainComponents} from 'dispatcher';
import {ajax} from 'utils';
import noty from 'noty';

export default class MakeOrder extends React.Component {

  constructor() {
    super();
    dispatcher['makeOrder'] = this;
    this.state = {
      post_id: null,
      description: ''
    };
  }

  changeDescription = (e) => {
    if (e.target.value.length < 400) {
      this.setState({
        description: e.target.value
      });
    }
  }

  makeOrder = () => {       
    ajax (
      '/order/create/',
      {
        post_id: this.state.post_id,
        description: this.state.description,
      },
      (data) => {
        if (data.error) {
          var n = noty({
            text: 'Зараз ця функція недоступна, cпробуйте пізніше або напишіть нам про проблему iqsk81ad@gmail.com',
            type: 'warning',
            animation: {
              open: 'animated bounceInLeft',
              close: 'animated bounceOutLeft',
              easing: 'swing',
              speed: 500
            },
            timeout: 5000,
          });
        } else {
          var n = noty({
            text: 'Ваша заява прийнята, найближчим часом з вами зв`яжуться',
            type: 'success',
            animation: {
              open: 'animated bounceInLeft',
              close: 'animated bounceOutLeft',
              easing: 'swing',
              speed: 500
            },
            timeout: 5000,
          });
          document.getElementById('makeOrder').style.display = 'none';
          document.getElementById('lean_overlay').style.display = 'none';
          let orders = JSON.parse(localStorage.orders);
          orders.push(this.state.post_id);
          localStorage.orders = JSON.stringify(orders);
          mainComponents.update();
        }
      }
    );
  }
  
  render() {
    return (
      <div className="makeOrder" id="makeOrder">
        <h2>Оформлення заявки на тварину</h2>
        <h3>Для того, щоб вам та тварині було комфортно, нам необхідно знати додаткову інформацію</h3>
        <div className="input-group">
          <label className="input-group__title" htmlFor="description">Додаткова інформація</label>
          <textarea value={this.state.description} className="input" type="description" id="description" placeholder="Введіть додаткову інформацію" onChange={this.changeDescription} />
        </div>
        <button className="button button_green" onClick={this.makeOrder}>Оформити заявку</button>
      </div>
    );
  }

}