'use strict'

import dispatcher from 'dispatcher';
import Header from './header/header';
import Footer from './footer/footer';
import ModalService from './modalService/modalService';

export default class MainComponent extends React.Component {

  constructor() {
    super();
    dispatcher['mainComponents'] = this;
    try {
      JSON.parse(localStorage.orders)
    } catch (e) {
      localStorage.orders = JSON.stringify([]);
    }
  }

  update = () => {
    this.forceUpdate();
    for (let key in dispatcher) {
      dispatcher[key].forceUpdate();
    }
  } 
  
  render() {
    return (
      <div className="frame">
        <div className="frame__row">
          <Header />
        </div>
        <div className="frame__row frame__row_content">
          {this.props.children}
        </div>
        <div className="frame__row">
          <Footer />
        </div>
        <ModalService />
      </div>
    );
  }

}