'use strict'

import dispatcher, {mainComponents, post} from 'dispatcher';
import {ajax} from 'utils';

export default class Header extends React.Component {

  componentDidMount() {
    $("#_signIn").leanModal();
  }

  componentDidUpdate() {
    this.componentDidMount();
  }

  createPost = () => {
    if (window.login) {
      mainComponents.props.history.pushState(null, '/createPost', null)
    } else {
      $("#_signIn").click();
    }
  }

  logout = () => {
    ajax (
      '/user/logout/',
      null,
      () => {
        window.login = false;
        window.role = '';
        mainComponents.update();
        if (post) {
          post.forceUpdate();
        }
      },
      'GET'
    );
  }

  checkLoggin() {
    if (window.login) {
      if (window.role === 'admin') {
        return [
          <li className="menu__item" key={1}><a onClick={() => {mainComponents.props.history.pushState(null, `/profile/${window.login}`, null)}} className="menu__link">Профіль</a></li>,
          <li className="menu__item" key={2}><a onClick={() => {mainComponents.props.history.pushState(null, `/validateOrder`, null)}} className="menu__link">Заяви</a></li>,
          <li className="menu__item" key={3}><a onClick={() => {mainComponents.props.history.pushState(null, `/validatePost`, null)}} className="menu__link">Пости</a></li>,
          <li className="menu__item" key={4}><a className="menu__link" onClick={this.logout}>Вихід</a></li>,
        ];
      } else {
        return [
          <li className="menu__item" key={1}><a onClick={() => {mainComponents.props.history.pushState(null, `/profile/${window.login}`, null)}} className="menu__link">Профіль</a></li>,
          <li className="menu__item" key={2}><a className="menu__link" onClick={this.logout}>Вихід</a></li>,
        ];
      }
    } else {
      return (
        <li className="menu__item"><a id="_signIn" className="menu__link" href="#signIn">Вхід</a></li>
      );
    }
  }
  
  render() {
    return (
      <header className="header header_relative">
        <a className="header__logo" onClick={() => {mainComponents.props.history.pushState(null, '/', null)}}></a>
        <div className="header__title">
          <span className="header__subname">Дитяча міська громадська організація</span><br />
          <span className="header__name">«Асоціація захисту тварин»</span>
        </div>
        <div className="menu">
          <nav className="menu__container">
            <li className="menu__item"><a onClick={() => {mainComponents.props.history.pushState(null, '/animals', null)}} className="menu__link">Тварини</a></li>
            <li className="menu__item"><a className="menu__link" onClick={this.createPost}>Створити пост</a></li>
            <li className="menu__item"><a onClick={() => {mainComponents.props.history.pushState(null, '/about', null)}} className="menu__link">Про нас</a></li>
            {
              this.checkLoggin()
            }
          </nav>
        </div>
      </header>
    );
  }

}