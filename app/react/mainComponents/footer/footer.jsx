'use strict'

import dispatcher, {mainComponents} from 'dispatcher';

export default class Footer extends React.Component {

  constructor() {
    super();
    dispatcher['footer'] = this;
    this.state = {

    };
  }

  goUp = () => {
   $('html, body').animate({
      scrollTop: 0
    }, 500);
  }
  
  render() {
    return (
      <footer>
        <div className="footer-main">
          <div className="footer-main__info">
            <div className="footer-main__title">AnimalsHelp</div>
            <div className="footer-main__note">Основною метою Асоціації є: виховання любові та гуманного ставлення до тварин, формування екологічної свідомості у дітей, допомога безпритульним тваринам.</div>
          </div>
          <div className="footer-main__menu">
            <ul className="footer-main__tiles">
              <li className="footer-main__tiles-item" onClick={() => {mainComponents.props.history.pushState(null, '/about', null)}}>Про нас</li>
              <li className="footer-main__tiles-item">Dashboard</li>
              <li className="footer-main__tiles-item">Feed</li>
              <li className="footer-main__tiles-item">Forums</li>
              <li className="footer-main__tiles-item">Journal</li>
              <li className="footer-main__tiles-item">Store</li>
            </ul>
            <ul className="footer-main__tiles footer-main_id_margin_left">
              <li className="footer-main__tiles-item" onClick={() => {mainComponents.props.history.pushState(null, '/animals', null)}}>Наші тварини</li>
              <li className="footer-main__tiles-item">Dashboard</li>
              <li className="footer-main__tiles-item">Feed</li>
              <li className="footer-main__tiles-item">Forums</li>
              <li className="footer-main__tiles-item">Journal</li>
              <li className="footer-main__tiles-item">Store</li>
            </ul>
          </div>
          <div className="top" onClick={this.goUp}></div>
        </div>
      </footer>
    );
  }

}