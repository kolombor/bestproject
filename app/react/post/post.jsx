'use strict'

import dispatcher, {mainComponents} from 'dispatcher';
import {ajax, makeType, makeAge} from 'utils';
import CreatePost from '../createPost/createPost';
import noty from 'noty';

export default class Post extends React.Component {

  constructor() {
    super();
    dispatcher['post'] = this;
    this.state = {
      postData: null,
      edit: false,
      delete: false,
    };
  }

  componentDidUpdate() {
    $('.pics').glisse({
      changeSpeed: 550, 
      speed: 500,
      effect:'bounce',
      fullscreen: true
    }); 
  }

  componentWillMount() {
    ajax (
      `/post/${this.props.params.id}/show/`,
      '',
      this.setPostData,
      'GET'
    );
  }

  setPostData = (data) => {
    if (data.error) {
      this.setState({
        postData: true,
        delete: true,
      });
    } else {
      var n = noty({
        text: 'Щоб переглянути всі фотографії натисніть на будь яку та керуйте стрілками клавіатури',
        type: 'information',
        animation: {
          open: 'animated bounceInLeft',
          close: 'animated bounceOutLeft',
          easing: 'swing',
          speed: 500
        },
        timeout: 5000,
      });
      this.setState({
        postData: data.post,
      });
    }
  }

  editPost = () => {
    this.setState({
      edit: true,
    });
  }

  changePost = () => {
    this.state.edit = false;
    this.componentWillMount();
  }

  changePublish = () => {
    ajax (
      `/post/${this.state.postData.id}/valid/`,
      {
        valid: !this.state.postData.valid,
      },
      (data) => {
        if (!data.error) {
          var n = noty({
            text: this.state.postData.valid ? 'Зняття з публікації пройшло успішно' : 'Публікація пройшла успішно',
            type: 'success',
            animation: {
              open: 'animated bounceInLeft',
              close: 'animated bounceOutLeft',
              easing: 'swing',
              speed: 500
            },
            timeout: 5000,
          });
          this.state.postData.valid = !this.state.postData.valid;
          this.forceUpdate();
        } else {
          var n = noty({
            text: 'Тимчасово ця функція недоступна, спробуйте пізніше або напишіть нам про проблему iqsk81ad@gmail.com',
            type: 'warning',
            animation: {
              open: 'animated bounceInLeft',
              close: 'animated bounceOutLeft',
              easing: 'swing',
              speed: 500
            },
            timeout: 5000,
          });
        }
      },
      'POST'
    );
  }

  deletePost = () => {
   ajax (
      `/post/${this.state.postData.id}/delete/`,
      '',
      (data) => {
        if (!data.error) {
          var n = noty({
            text: 'Пост успішно видалений',
            type: 'success',
            animation: {
              open: 'animated bounceInLeft',
              close: 'animated bounceOutLeft',
              easing: 'swing',
              speed: 500
            },
            timeout: 5000,
          });
          mainComponents.props.history.pushState(null, `/`, null);
        } else {
          var n = noty({
            text: 'Тимчасово ця функція недоступна, спробуйте пізніше або напишіть нам про проблему iqsk81ad@gmail.com',
            type: 'warning',
            animation: {
              open: 'animated bounceInLeft',
              close: 'animated bounceOutLeft',
              easing: 'swing',
              speed: 500
            },
            timeout: 5000,
          });
        }
        
      },
      'DELETE'
    );
  }

  makeOrder = () => {
    if (window.login) {
      dispatcher.makeOrder.setState({
        post_id: this.state.postData.id,
      });
      $('#_makeOrder').leanModal();
      $("#_makeOrder").click();
    } else {
      $("#_signIn").click();
    }

  }

  checkSelfPost() {
    if (window.login) {
      if (window.role === 'admin') {
        return [
          <button type="button" key={0} className="button button_yellow" onClick={this.changePublish}>{this.state.postData.valid ? 'Зняти з публікації' : 'Опублікувати'}</button>,
          <button style={{marginLeft: '20px'}} type="button" key={1} className="button button_red" onClick={this.deletePost}>Видалити пост</button>,
        ];
      } else {
        if (window.login == this.state.postData.user) {
          return [
            <button type="button" key={0} className="button button_blue" onClick={this.editPost}>Редагувати</button>,
            <button style={{marginLeft: '20px'}} type="button" key={1} className="button button_red" onClick={this.deletePost}>Видалити пост</button>,
          ];
        } else {
          if (JSON.parse(localStorage.orders).indexOf(this.state.postData.id) >= 0) {
            return (
              <button type="button" className="button button_green button_disabled">Заява опрацьовується</button>
            );
          } else {
            return [
              <a id="_makeOrder" href="#makeOrder" style={{display: 'none'}} key={0} />,
              <button type="button" className="button button_green" onClick={this.makeOrder} key={1}>Взяти тварину</button>,
            ];
          }
        }
      }
    } else {
      return [
        <a id="_makeOrder" href="#makeOrder" style={{display: 'none'}} key={0} />,
        <button type="button" className="button button_green" onClick={this.makeOrder} key={1}>Взяти тварину</button>,
      ];
    }
  }
  
  render() {
    if (this.state.postData) {
      if (this.state.edit) {
        return (
          <CreatePost data={this.state.postData} />
        );
      } else {
        if (this.state.delete) {
          return (
            <h2 className="post_deleted">Нажаль пост видалений</h2>
          );
        } else {
          return (
           <main>
              <div className="post">
                {
                  this.state.postData.photos.map((photo, key) => {
                    return (
                      <img style={{display: key ? 'none' : 'block'}} key={key} src={this.state.postData.photos[0]} width="600" className="pics" data-glisse-big={photo} rel="group1" />
                    );
                  })
                }
                <div className="post-group">
                  <label className="post-group__title" htmlFor="name">Назва поста</label>
                  <div className="post-group__data">{this.state.postData.name}</div>
                </div>
                <div className="post-group">
                  <label className="post-group__title" htmlFor="selectType">Тип тварини</label>
                  <div className="post-group__data">{makeType(this.state.postData.animal_type)}</div>
                </div>
                <div className="post-group">
                  <label className="post-group__title" htmlFor="selectAge">Вік тварини</label>
                  <div className="post-group__data">{makeAge(this.state.postData.age)}</div>
                </div>
                <div className="post-group">
                  <label className="post-group__title" htmlFor="description">Опис</label>
                  <div className="post-group__data">{this.state.postData.description}</div>
                </div>
                <div className="post__button">
                  {
                    this.checkSelfPost()
                  }
                </div>
              </div>
            </main>
          );
        }
      }
    } else {
      return null;
    }
  }

}