'use strict'

var webpack = require('webpack'),
  path = require('path'),
  ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {

  entry: {
    app: './app/react/reactApp.jsx',
  },

  output: {
    path: path.join(__dirname, '/static/build'),
    filename: 'bundle.js',
  },

  resolve: {
    extensions: ['', '.js', '.jsx', '.json'],
    alias: {
      'utils': __dirname + '/utils/utils',
      'dispatcher': __dirname + '/app/react/dispatcher/dispatcher',
    },
  },

  module: {
    loaders: [
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        loader: 'babel-loader?stage=0',
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader?stage=0',
      },
      {
        test: /\.scss/,
        loader: ExtractTextPlugin.extract('css-loader!sass-loader'),
      },
      {
        test   : /\.(png|jpeg|jpg|gif|ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        loader : 'file-loader'
      },
    ]
  },

  plugins: [
    new webpack.ProvidePlugin({
      React: 'react',
      ReactDOM: 'react-dom',
    }),
    new ExtractTextPlugin('style.css'),
  ],

  devtool: 'source-map'

}