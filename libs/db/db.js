'use strict'

import mongojs from 'mongojs';

let databaseUrl = "mydb"; // "username:password@example.com/mydb"
let collections = ["users", "reports"]

let db = mongojs(databaseUrl, collections);

export default db; 